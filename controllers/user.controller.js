const { user_bio, user_game, user_history } = require('../models');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const transportEmail = require('../misc/mailer');
require('dotenv').config();

const getAllUserData = async (req, res) => {
    let { page, row } = req.query;
    page -= 1;
    try {
        const options = {
            attributes: {
                exclude: ['createdAt', 'updatedAt', 'password']
            },
            include: [
                {
                    model: user_bio,
                    attributes: ['username', 'age', 'gender']
                },
                {
                    model: user_history,
                    attributes: ['game_played', 'time_played']
                }
            ],
            offset: page,
            limit: row,
        }

        const userData = await user_game.findAll(options);
        res.status(200).json({
            status: 'success',
            data: userData
        })
    } catch (err) {
        res.status(404).json({
            status: 'fail',
            message: 'User not found'
        })
    }
}

const getUserDataById = async (req, res) => {
    //getting user data by id
    try {
        const userData = await user_game.findOne({
            where: {
                id: req.params.id
            },
            attributes: {
                exclude: ['createdAt', 'updatedAt', 'password']
            },
        });
        if (!userData) {
            res.status(404).json({
                status: 'fail',
                message: 'Data tidak ditemukan'
            })
        } else {
            res.status(200).json({
                status: 'success',
                data: userData
            })
        }
    } catch (err) {
        console.log(err);
        throw new Error(err);
    }
}

const createUserGame = async (req, res) => {
    try {
        const createdUserGame = await user_game.create({
            email: req.body.email,
            password: req.body.password
        });
        if (createdUserGame) {
            await user_bio.create({
                username: req.body.username,
                age: req.body.age,
                gender: req.body.gender,
                user_id: createdUserGame.id
            }),
                await user_history.create({
                    game_played: req.body.game_played,
                    time_played: req.body.time_played,
                    user_id: createdUserGame.id
                })
        };

        const clearCreate = await user_game.findOne({
            where: {
                id: createdUserGame.id
            },
            include: [{
                model: user_bio,
                model: user_history
            }]
        });
        const { email } = req.body;
        const greet = `Thank you for registering your account in our website`;
        const emailResponse = await transportEmail(email, greet);
        res.status(201).json({
            status: 'User Created Success',
            data: clearCreate.id,
            message: `Email sent to ${emailResponse.accepted.join(',').split(',')}`
        });

    } catch (err) {
        console.log(err);
    }
}
//still confusing on update feature
const updateUserGame = async (req, res, next) => {
    try {
        // const whereClause = { where: {id: req.params.id} }
        const updateUser = await user_game.update({
            email: req.body.email,
            password: req.body.password,
            updatedAt: new Date()
        }, { where: { id: req.params.id } })

        // console.log(updateUser.id);
        if (updateUser) {
            await user_bio.update({
                username: req.body.username,
                age: req.body.age,
                gender: req.body.gender,
                updatedAt: new Date()
            }, { where: { id: req.params.id } }),

                await user_history.update({
                    game_played: req.body.game_played,
                    time_played: req.body.time_played,
                    updatedAt: new Date()
                }, { where: { id: req.params.id } })
        };
        const clearUpdate = await user_game.findOne({
            where: {
                id: req.params.id
            },
            include: [{
                model: user_bio,
                model: user_history
            }]
        });
        res.status(200).json({
            status: 'success',
            data: clearUpdate
        })
    } catch (err) {
        next(err);
    }
}

const deleteUserGame = async (req, res) => {
    try {
        const deleteUserGame = await user_game.destroy({
            where: {
                id: req.params.id
            }
        });
        if (deleteUserGame) {
            await user_bio.destroy({
                where: {
                    user_id: req.params.id
                }
            }),
                await user_history.destroy({
                    where: {
                        user_id: req.params.id
                    }
                })
        };
        if (!deleteUserGame) {
            res.status(404).json({
                status: 'fail',
                message: 'Data tidak ditemukan'
            })
        } else {
            res.status(200).json({
                status: 'success',
                data: deleteUserGame
            })
        }
    } catch (err) {
        console.log(err);
        throw new Error(err);
    }
}


const loginUserGame = async (req, res) => {
    try {
        const { email, password } = req.body;
        const gotUser = await user_game.findOne({
            where: {
                email: email
            }
        })
        const validUser = bcrypt.compareSync(password, gotUser.password);

        if (!gotUser) {
            res.status(404).json({
                status: 'fail',
                message: 'Email not found'
            })
        } else if (!validUser) {
            res.status(400).json({
                status: 'fail',
                message: 'Password not match'
            })
        }

        if (validUser) {
            const payload = {
                id: gotUser.id,
                email: gotUser.email,
                iat: Date.now()
            }
            const token = jwt.sign(payload, process.env.KEY_JWT, { expiresIn: '1h' });

            res.status(200).json({
                token: token
            })
        } else {
            res.status(400).json({
                message: 'Failed Login'
            })
        }
    } catch (err) {
        console.log(err);
        throw new Error(err);
    }
}

const serviceUpload = async (req, res) => {
    try {
        const { video } = req.body;
        res.status(200).json({
            status: 'success',
            data: video
        })
    } catch (err) {
        console.log(err);
        throw new Error(err);
    }
}
// testing the send email
// const sendEmail = async (req, res) => {
//     try {
//         const { email } = req.body;
//         const emailResponse = await transportEmail(email);
//         return res.status(200).json({
//             message: `Email sent to ${emailResponse.accepted.join(',').split(',')}`
//         });
//     } catch (err) {
//         res.status(400).json({
//             message: 'Input back the email you want to send'
//         })
//     }
// }

const otpGenerate = async (req, res, next) => {
    try {
        const { email } = req.body;
        const otp = Math.random().toString(36).slice(-6).toUpperCase();
        // hashing the otp
        const otpAfterHash = bcrypt.hashSync(otp, +process.env.SALT_ROUNDS);
        // saving the otp to database
        const saveOtp = await user_game.update(
            {
                otp: otpAfterHash
            }, {
            where: {
                email: email
            }
        });
        if (saveOtp) {
            const otpMessage = `Your OTP is ${otp}, remember your OTP to change the Password`;

            const emailResponse = await transportEmail(email, otpMessage);

            return res.status(200).json({
                message: `OTP sent to ${emailResponse.accepted.join(',').split(',')}`
            });
        } else {
            throw {
                message: 'Email not found',
                status: 'Failed',
                code: 404
            }
        };
    } catch (err) {
        if (err.code) {
            return res.status(err.code).json({
                message: err.message,
                status: err.status
            })
        }
        next(err);
    }
}

const forgetPassword = async (req, res, next) => {
    try {
        const { email, newPassword, newPasswordConfirmation, otp } = req.body;

        if (newPassword === newPasswordConfirmation) {
            const getUser = await user_game.findOne({
                where: {
                    email: email
                }
            })

            if (!getUser) {
                throw {
                    message: 'Email not found',
                    status: 'Failed',
                    code: 404
                }
            }

            const validOtp = bcrypt.compareSync(otp, getUser.otp);

            if (validOtp) {
                await getUser.update({
                    password: bcrypt.hashSync(newPassword, +process.env.SALT_ROUNDS),
                    otp: null
                })
                return res.status(200).json({
                    status: 'success',
                    message: 'Password changed!'
                })
            }
        }
        throw {
            message: 'Password doesnt match',
            status: 'Failed',
            code: 400
        }
    } catch (err) {
        if (err.code) {
            return res.status(err.code).json({
                message: err.message,
                status: err.status
            })
        }
        next(err);
    }
}

module.exports = {
    getAllUserData,
    getUserDataById,
    createUserGame,
    updateUserGame,
    deleteUserGame,
    loginUserGame,
    serviceUpload,
    otpGenerate,
    forgetPassword
}