const { user_bio } = require('../models');

const getUserBioData = async (req, res) => {
    let { page, row } = req.query;
    page -= 1;
    try {
        const options = {
            attributes: ['id', 'username', 'age', 'gender'],
            offset: page,
            limit: row
        }
        const userData = await user_bio.findAll(options);
        res.status(200).json({
            status: 'success',
            data: userData
        })
    } catch {
        res.status(404).json({
            status: 'fail',
            message: 'User not found'
        })
    }
}

const getUserBioDataById = async (req, res) => {
    try {
        const userBiodata = await user_bio.findOne({
            where: {
                id: req.params.id
            },
            attributes: ['id', 'username', 'age', 'gender'],
        });
        if (!userBiodata) {
            res.status(404).json({
                status: 'fail',
                message: 'Data tidak ditemukan'
            })
        } else {
            res.status(200).json({
                status: 'success',
                data: userBiodata
            })
        }
    } catch (err) {
        console.log(err);
        throw new Error(err);
    }
}

const updateUserBioData = async (req, res, next) => {
    const { username, age, gender } = req.body;
    try {
        const updatedUserBio = await user_bio.update({
            username,
            age,
            gender
        }, {
            where: {
                id: req.params.id
            },
            plain: true,
            returning: true
        });
        res.status(200).json({
            status: 'success',
            data: updatedUserBio[1]
        })
    } catch (err) {
        // console.log(err);
        next(err);
    }
}

const deleteUserBioData = async (req, res) => {
    try {
        const deletedUserBio = await user_bio.destroy({
            where: {
                id: req.params.id
            }
        });
        if (!deletedUserBio) {
            res.status(404).json({
                status: 'fail',
                message: 'Data tidak ditemukan'
            })
        } else {
            res.status(200).json({
                status: 'success',
                data: deletedUserBio
            })
        }
    } catch (err) {
        console.log(err);
        throw new Error(err);
    }
}

module.exports = {
    getUserBioData,
    getUserBioDataById,
    updateUserBioData,
    deleteUserBioData
}