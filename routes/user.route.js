const express = require('express');
const router = express.Router();
const authValid = require('../misc/validation');
const upload = require('../misc/defaultFile');
const uploadVideo = require('../misc/uploadService');
const { getAllUserData, createUserGame, getUserDataById, updateUserGame, deleteUserGame, loginUserGame, serviceUpload, otpGenerate, forgetPassword } = require('../controllers/user.controller')


router.get('/', authValid, getAllUserData);
router.get('/:id', authValid, getUserDataById);

router.post('/', createUserGame); //here for register and get the email

router.put('/:id', authValid, updateUserGame);
router.delete('/:id', authValid, deleteUserGame);
//for login
router.post('/login', loginUserGame);
//for upload service
router.post('/upload-video', upload.single('video'), uploadVideo, serviceUpload);
// OTP GENERATE
router.post('/otp-generate', otpGenerate)
// Forget Password
router.post('/forget-password', forgetPassword);
// //test
// router.post('/sendEmail', sendEmail);

module.exports = router;