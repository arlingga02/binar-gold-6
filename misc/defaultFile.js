const multer = require('multer');
const storage = multer.diskStorage({
    filename: (req, file, cb) => {
        const fileFormat = file.mimetype.split('/')[1];
        cb(null, `${file.fieldname}-${Date.now()}.${fileFormat}`);
    }
});

const uploadFile = multer({
    storage: storage,
    fileFilter: (req, file, cb) => {
        if (file.mimetype === 'video/mp4') {
            return cb(null, true);
        } else {
            return cb(new Error('File must be mp4'), false);
        }
    },
    limits: {
        fileSize: 10000000
    }
})

module.exports = uploadFile;
