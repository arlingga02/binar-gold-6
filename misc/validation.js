require('dotenv').config();
const { user_game } = require('../models');
const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt;
const opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = process.env.KEY_JWT;

passport.use(
    new JwtStrategy(opts, async (jwt_payload, done) => {
        try {
            if (Date.now() < jwt_payload.exp) {
                throw new Error('Token Expired');
            } else {
                const user = await user_game.findOne({
                    where: {
                        id: jwt_payload.id,
                        email: jwt_payload.email
                    }
                })
                return done(null, user);
            }
        } catch (err) {
            console.log(err);
            return done(err, false);
        }
    })
)

module.exports = passport.authenticate('jwt', { session: false });

