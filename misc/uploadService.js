const cloudinary = require('cloudinary').v2;
const fs = require('fs');

cloudinary.config({
    cloud_name: 'dcofa7u5a',
    api_key: '756446543595635',
    api_secret: 'u6uW6Tj8EbZYnbmcTWx_bW7CF9I'
});


const uploadVideo = async (req, res, next) => {
    try {
        const folderPath = `my_data/video/${req.file.mimetype.split('/')[1]}`;
        console.log(req.file.path);
        const uploadVideo = await cloudinary.uploader.upload(req.file.path, {
            folder: folderPath,
            resource_type: 'video'
        });
        fs.unlinkSync(req.file.path);
        req.body.video = uploadVideo.secure_url;
        next();
    } catch (err) {
        fs.unlinkSync(req.file.path);
        console.log(err)
    }
}

module.exports = uploadVideo;