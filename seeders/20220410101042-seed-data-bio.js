'use strict';
const userBio = require('../masterdata/user_bios.json')


module.exports = {
  async up (queryInterface, Sequelize) {
      const userBioDataToSeed = userBio.map((eachUserBio) => {
        return {
          username: eachUserBio.username,
          age: eachUserBio.age,
          gender: eachUserBio.gender,
          user_id: eachUserBio.user_id,
          createdAt: new Date(),
          updatedAt: new Date(),
        }
      })
      await queryInterface.bulkInsert('user_bios', userBioDataToSeed, {});
    
},
  async down (queryInterface, Sequelize) {
      await queryInterface.bulkDelete('user_bios', null, { truncate: true, restartIdentity: true });
  }
};
