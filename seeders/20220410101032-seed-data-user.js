'use strict';
const userGame = require('../masterdata/user_games.json')
const bcrypt = require('bcrypt');
require('dotenv').config();

const userGameDataToSeed = userGame.map(async (eachUserGame) => {
  eachUserGame.email = eachUserGame.email;
  eachUserGame.password = await bcrypt.hash(eachUserGame.password, +process.env.SALT_ROUNDS);
  eachUserGame.createdAt = new Date();
  eachUserGame.updatedAt = new Date();
  return eachUserGame;
})

module.exports = {
  async up(queryInterface, Sequelize) {
    const userGameDataHash = await Promise.all(userGameDataToSeed)
    await queryInterface.bulkInsert('user_games', userGameDataHash, {});
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('user_games', null, { truncate: true, restartIdentity: true });
  }
};
