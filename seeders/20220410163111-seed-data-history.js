'use strict';
const userHistory = require('../masterdata/user_histories.json')

module.exports = {
  async up (queryInterface, Sequelize) {
    const userHistoryDataToSeed = userHistory.map((eachUserHistory) => {
      return {
        game_played: eachUserHistory.game_played,
        time_played: eachUserHistory.time_played,
        user_id: eachUserHistory.user_id,
        createdAt: new Date(),
        updatedAt: new Date(),
      }
      
    })
    await queryInterface.bulkInsert('user_histories', userHistoryDataToSeed, {});
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('user_histories', null, { truncate: true, restartIdentity: true });
  }
};
