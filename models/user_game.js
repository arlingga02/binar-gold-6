'use strict';

const bcrypt = require('bcrypt');
require('dotenv').config();
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_game extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      user_game.hasOne(models.user_bio, {
        foreignKey: 'user_id',
      }),
        user_game.hasMany(models.user_history, {
          foreignKey: 'user_id',
        })
    }
  }
  user_game.init({
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    otp: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'user_game',
    hooks: {
      beforeCreate: async (user_game, options) => {
        user_game.password = await bcrypt.hash(user_game.password, +process.env.SALT_ROUNDS);
        return user_game;
      },
      beforeUpdate: async (user_game, options) => {
        user_game.password = await bcrypt.hash(user_game.password, +process.env.SALT_ROUNDS);
        return user_game;
      }
    }
  });
  return user_game;
};