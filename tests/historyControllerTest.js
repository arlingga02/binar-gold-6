//make integration testing in bio controller

const historyTesting = () => {

    require('dotenv').config();
    const baseURL = process.env.BASE_URL || '/workspace';
    const app = require('../app');
    const request = require('supertest');

    //positive test
    describe('GET all data history user', () => {
        it('should get all data user history', async () => {
            const res = await request(app).get(`${baseURL}/history?page=1&row=2`);
            const { status, data } = res.body;
            expect(res.status).toBe(200);
            expect(status).toBe('success');
            expect(data.length).toBeGreaterThan(0);
        })
    });

    describe('GET data user history by id', () => {
        it('should get data by id', async () => {
            const res = await request(app).get(`${baseURL}/history/1`);
            const { status, data } = res.body;
            expect(res.status).toBe(200);
            expect(status).toBe('success');
            expect(data).toBeDefined();
        })
    })

    describe('UPDATE data history by id', () => {
        it('should update data history by id', async () => {
            const res = await request(app).put(`${baseURL}/history/2`).send({
                game_played: 55,
                time_played: 20
            });
            const data = res.body;
            expect(res.status).toBe(200);
            expect(data.status).toBe('success');
            expect(data).toMatchObject({
                status: "success",
                data: {
                    id: 2,
                    game_played: 55,
                    time_played: 20,
                    user_id: 2,
                    createdAt: data.data.createdAt,
                    updatedAt: data.data.updatedAt
                }
            });
        })
    })

    //negative test
    describe('GET all data history but there.s no data', () => {
        it('should get all data but there.s no data', async () => {
            const res = await request(app).get(`${baseURL}/history`);
            const { status, data } = res.body;
            expect(res.status).toBe(404);
            expect(status).toBe('fail');
            expect(data).toBeUndefined();
        })
    })

    describe('DELETE data but there.s no id', () => {
        it('should delete data but there.s no id', async () => {
            const res = await request(app).delete(`${baseURL}/history/0`);
            const { status, data } = res.body;
            expect(res.status).toBe(404);
            expect(status).toBe('fail');
            expect(data).toBeUndefined();
        })
    })

    describe('GET data history by id, but wrong', () => {
        test('should get data by id and not found', async () => {
            const res = await request(app).get(`${baseURL}/history/0`);
            const { status, data } = res.body;
            expect(res.status).toBe(404);
            expect(status).toBe('fail');
            expect(data).toBeUndefined();
        })
    })

    describe('UPDATE data history by id, but id wrong', () => {
        it('should update but the bio is wrong', async () => {
            const res = await request(app).put(`${baseURL}/history/0`).send({
                game_played: 55,
                time_played: 20
            });
            const data = res.body;
            expect(res.status).toBe(404);
            expect(data.status).toBe('Id Not Found');
            expect(data).toMatchObject({
                status: 'Id Not Found',
                message: 'No Id You Need'
            });
        })
    })
}

module.exports = historyTesting;