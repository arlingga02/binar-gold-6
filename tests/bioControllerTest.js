//make integration testing in bio controller

const bioTesting = () => {

    require('dotenv').config();
    const baseURL = process.env.BASE_URL || '/workspace';
    const app = require('../app');
    const request = require('supertest');

    //positive test
    describe('GET all data bio', () => {
        it('should get all data bio', async () => {
            const res = await request(app).get(`${baseURL}/bio?page=1&row=2`);
            const { status, data } = res.body;
            expect(res.status).toBe(200);
            expect(status).toBe('success');
            expect(data.length).toBeGreaterThan(0);
        })
    });

    describe('GET data bio by id', () => {
        it('should get data by id', async () => {
            const res = await request(app).get(`${baseURL}/bio/1`);
            const { status, data } = res.body;
            expect(res.status).toBe(200);
            expect(status).toBe('success');
            expect(data).toBeDefined();
        })
    })

    describe('UPDATE data bio by id', () => {
        it('should update data by id', async () => {
            const res = await request(app).put(`${baseURL}/bio/2`).send({
                username: 'dicky222',
                age: "20",
                gender: "man"
            });
            const data = res.body;
            expect(res.status).toBe(200);
            expect(data.status).toBe('success');
            expect(data).toMatchObject({
                status: "success",
                data: {
                    id: 2,
                    username: "dicky222",
                    age: "20",
                    gender: "man",
                    user_id: 2,
                    createdAt: data.data.createdAt,
                    updatedAt: data.data.updatedAt
                }
            });
        })
    })

    //negative test
    describe('GET all data but there.s no user', () => {
        it('should get all data but there.s no user', async () => {
            const res = await request(app).get(`${baseURL}/bio`);
            const { status, data } = res.body;
            expect(res.status).toBe(404);
            expect(status).toBe('fail');
            expect(data).toBeUndefined();
        })
    })

    describe('DELETE data but there.s no id', () => {
        it('should delete data but there.s no id', async () => {
            const res = await request(app).delete(`${baseURL}/bio/0`);
            const { status, data } = res.body;
            expect(res.status).toBe(404);
            expect(status).toBe('fail');
            expect(data).toBeUndefined();
        })
    })

    describe('GET data bio by id, but wrong', () => {
        test('should get data by id and not found', async () => {
            const res = await request(app).get(`${baseURL}/bio/0`);
            const { status, data } = res.body;
            expect(res.status).toBe(404);
            expect(status).toBe('fail');
            expect(data).toBeUndefined();
        })
    })

    describe('UPDATE data bio by id, but id wrong', () => {
        it('should update but the bio is wrong', async () => {
            const res = await request(app).put(`${baseURL}/bio/0`).send({
                username: 'dicky222',
                age: "20",
                gender: "man"
            });
            const data = res.body;
            expect(res.status).toBe(404);
            expect(data.status).toBe('Id Not Found');
            expect(data).toMatchObject({
                status: 'Id Not Found',
                message: 'No Id You Need'
            });
        })
    })
}

module.exports = bioTesting;