//make integration testing in bio controller

const userTesting = () => {

    require('dotenv').config();
    const baseURL = process.env.BASE_URL || '/workspace';
    const app = require('../app');
    const request = require('supertest');

    //positive test
    describe('GET all data user', () => {
        it('should get all data user', async () => {
            const res = await request(app).get(`${baseURL}/user?page=1&row=2`);
            const { status, data } = res.body;
            expect(res.status).toBe(200);
            expect(status).toBe('success');
            expect(data.length).toBeGreaterThan(0);
        })
    });

    describe('GET data user by id', () => {
        it('should get data by id', async () => {
            const res = await request(app).get(`${baseURL}/user/1`);
            const { status, data } = res.body;
            expect(res.status).toBe(200);
            expect(status).toBe('success');
            expect(data).toBeDefined();
        })
    })

    //negative test
    describe('GET all data user but there.s no data', () => {
        it('should get all data but there.s no data', async () => {
            const res = await request(app).get(`${baseURL}/user`);
            const { status, data } = res.body;
            expect(res.status).toBe(404);
            expect(status).toBe('fail');
            expect(data).toBeUndefined();
        })
    })

    describe('GET data history by id, but wrong', () => {
        test('should get data by id and not found', async () => {
            const res = await request(app).get(`${baseURL}/user/0`);
            const { status, data } = res.body;
            expect(res.status).toBe(404);
            expect(status).toBe('fail');
            expect(data).toBeUndefined();
        })
    })

    describe('DELETE data but there.s no id', () => {
        it('should delete data but there.s no id', async () => {
            const res = await request(app).delete(`${baseURL}/user/0`);
            const { status, data } = res.body;
            expect(res.status).toBe(404);
            expect(status).toBe('fail');
            expect(data).toBeUndefined();
        })
    })
}

module.exports = userTesting;