const { sequelize } = require('../models');

const seeding = (path) => {
    return require(path).map((eachData) => {
        eachData.createdAt = new Date();
        eachData.updatedAt = new Date();
        return eachData;
    });
}

const userData = seeding('../masterdata/user_games.json');
const bioData = seeding('../masterdata/user_bios.json');
const historyData = seeding('../masterdata/user_histories.json');

beforeAll(async () => {
    try {
        await sequelize.queryInterface.bulkInsert('user_games', userData, {});
        await sequelize.queryInterface.bulkInsert('user_bios', bioData, {});
        await sequelize.queryInterface.bulkInsert('user_histories', historyData, {});
    } catch (error) {
        console.log(error);
    }
})
afterAll(async () => {
    try {
        await sequelize.queryInterface.bulkDelete('user_histories', null, { truncate: true, cascade: true, restartIdentity: true });
        await sequelize.queryInterface.bulkDelete('user_bios', null, { truncate: true, cascade: true, restartIdentity: true });
        await sequelize.queryInterface.bulkDelete('user_games', null, { truncate: true, cascade: true, restartIdentity: true });
        await sequelize.close();
    } catch (error) {
        console.log(error);
    }
})

const userTesting = require('./userControllerTest');
const bioTesting = require('./bioControllerTest');
const historyTest = require('./historyControllerTest');

describe('running the test', () => {
    userTesting();
    bioTesting();
    historyTest();
});