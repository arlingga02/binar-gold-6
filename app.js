require('dotenv').config();
const express = require('express');
const router = require('./routes/index.route');
const app = express();
const port = process.env.PORT;
const morgan = require('morgan');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(morgan('dev'));

app.use(`${process.env.BASE_URL}`, router); //  /workspace

app.use((err, req, res, next) => {
    if (err === 'Data tidak ada') {
        res.status(404).json({
            status: 'Bad Request',
            message: err
        })
    } else if (err === `Bad for Creating`) {
        res.status(400).json({
            status: 'Not Found',
            message: err.message
        });
    } else if (err.name === 'TypeError') {
        res.status(404).json({
            status: 'There is no such data',
            message: 'Check again the email or id'
        })
    } else if (err.name === 'Token Expired') {
        res.status(400).json({
            status: 'Failed',
            message: err.message
        });
    }
    else {
        res.status(500).json({
            status: err.name,
            message: err.message
        });
    }

})


module.exports = app;